package sample;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import sample.king.App;

import java.io.IOException;
import java.util.List;

public class Controller {

    final static Clipboard clipboard = Clipboard.getSystemClipboard();
    final static ClipboardContent content = new ClipboardContent();
    @FXML
    public TextField searchField;
    @FXML
    public Button searchButton;
    @FXML
    public Button createButton;
    @FXML
    public ListView<String> outputList;

    public void initialize() {

        boolean doFileExists = App.doFileExists();
        if (doFileExists) {
            searchButton.setDisable(false);
            createButton.setText("Refresh Tree");
        } else {
            searchButton.setDisable(true);
        }
        outputList.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Copy \"%s\"", cell.itemProperty()));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                System.out.println(item);
                content.putString(item);
                clipboard.setContent(content);
            });
            contextMenu.getItems().addAll(editItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell;
        });
        System.out.println("Initialize");
    }

    public void searchClicked() throws IOException {
        App.createOrRefreshTree(false);
        String packageName = searchField.getText();
        List<String> paths = App.getPathToJars(packageName);
        if (paths.isEmpty()) {
            setListView("NONE FILE CONTAIN THIS PACKAGE");
        } else {
            setListView(App.getPathToJars(packageName));
        }
    }

    public void createOrRefreshClicked() throws IOException {
        boolean doFileExists = App.doFileExists();
        if (doFileExists) {
            App.createOrRefreshTree(true);
        } else {
            App.createOrRefreshTree(true);
            searchButton.setDisable(false);
            createButton.setText("Refresh Tree");
        }
    }

    public void setListView(List<String> fields) {
        ObservableList<String> items = FXCollections.observableArrayList(fields);
        outputList.setItems(items);
    }

    public void setListView(String field) {
        ObservableList<String> items = FXCollections.observableArrayList(field);
        outputList.setItems(items);
    }

}
