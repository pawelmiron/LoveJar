package sample.king;


import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import sample.king.model.JarModel;
import sample.king.model.Node;
import sample.king.utility.FilesReviewer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Main application class for running search
 *
 * @author Pawel Miron
 */
public class App {

    private static final Logger LOGGER = Logger.getLogger(App.class.getName());
    private static Node root = new Node(null, new ArrayList<Node>());
    private static Node rootNode;

    public static void createOrRefreshTree(boolean clean) throws IOException {
        long start = System.currentTimeMillis();
        List<String> listOfSearchPaths = new ArrayList<>();
        listOfSearchPaths.add("E:\\Windchill_11.2\\Windchill");
        try {
            rootNode = prepareTree(listOfSearchPaths, clean);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        long end = System.currentTimeMillis();
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");
    }

    /**
     * Method returns sorted tree where leafs represents packages and nodes represents path
     *
     * @param listOfSearchPaths list of paths from with tree will be build
     * @return tree created based on given paths or tree created before
     * @throws IOException
     */
    public static Node prepareTree(List<String> listOfSearchPaths, Boolean clean) throws IOException {
        LOGGER.info("Checking existence of build tree");
        boolean doFileExists = doFileExists();
        if (doFileExists && !clean) {
            LOGGER.info("Using created tree");
            return root = deserializeFile(getSaveFilePath());
        }
        LOGGER.info("Started preparing tree, this could take some time");
        List<JarModel> all = new ArrayList<>();
        for (String path : listOfSearchPaths) {
            all.addAll(FilesReviewer.getAllDirectoryFiles(path));
        }
        for (JarModel jarModel : all) {
            List<String> listOfJarsPathsToClasses = jarModel.getJarFileList();
            for (String packageName : listOfJarsPathsToClasses) {
                List<String> splitList = new ArrayList<>(Arrays.asList(packageName.split("/")));
                addToNodeTree(jarModel.getJarPath(), splitList, root);
            }
        }
        if (doFileExists) {
            LOGGER.info("Over writing existing tree file with a new one");
        } else {
            LOGGER.info("Creating new tree file");
        }
        serializeObject(root);
        return root;

    }

    public static boolean doFileExists() {
        return new File(getSaveFilePath()).exists();
    }


    private static void addToNodeTree(String jarPath, List<String> splitList, Node node) {
        if (!splitList.isEmpty()) {
            Node node2 = getNodeWithName(splitList.get(0), node);
            if (node2 != null) {
                splitList.remove(0);
                addToNodeTree(jarPath, splitList, node2);
            } else {
                Node node3 = new Node(splitList.get(0), new ArrayList<Node>());
                node.getChildren().add(node3);
                splitList.remove(0);
                addToNodeTree(jarPath, splitList, node3);
            }
        } else {
            node.getPathsToJars().add(jarPath);
        }

    }

    private static Node getNodeWithName(String name, Node node) {
        for (Node checkedNode : node.getChildren()) {
            if (checkedNode.getName().equals(name)) {
                return checkedNode;
            }
        }
        return null;
    }

    public static List<String> getPathToJars(String packageName) {
        LOGGER.info("Searching for files containing package with name: " + packageName);
        packageName = packageName.replace('.', '/');
        packageName = packageName + ".class";
        List<String> splitList = new ArrayList<>(Arrays.asList(packageName.split("/")));
        Node node = rootNode;
        while (!splitList.isEmpty()) {
            if (node != null) {
                node = getNodeWithName(splitList.get(0), node);
                splitList.remove(0);
            } else {
                throw new NullPointerException();
            }

        }
        if (node == null) {
            return new ArrayList<>();
        }
        LOGGER.info("Found these jars:");
        for (String jar : Objects.requireNonNull(node).getPathsToJars()) {
            LOGGER.info(" - " + jar);
        }
        return node.getPathsToJars();
    }

    private static void serializeObject(Node o) throws IOException {
        byte[] yourBytes = SerializationUtils.serialize(o);
        if (new File(getSavePath()).mkdirs()) {
            LOGGER.info("Directory exists");
        }
        try (FileOutputStream fos = new FileOutputStream(getSaveFilePath())) {
            fos.write(yourBytes);
        }
    }

    private static Node deserializeFile(String pathString) throws IOException {
        Path path = Paths.get(pathString);
        byte[] data = Files.readAllBytes(path);
        return (Node) SerializationUtils.deserialize(data);
    }

    private static String getSavePath() {
        return System.getProperty("user.home") + "\\AppData\\Local\\MironFinder";
    }

    private static String getSaveFilePath() {
        return System.getProperty("user.home") + "\\AppData\\Local\\MironFinder\\tree.bin";
    }

}
