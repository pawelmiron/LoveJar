package sample.king.model;

import org.apache.log4j.Logger;

import java.util.List;

/**
 * Created by Pawel Miron on 04.07.2018.
 */
public class JarModel {

    private static final Logger LOGGER = Logger.getLogger(JarModel.class.getName());

    private String jarPath;
    private List<String> jarFileList;

    public JarModel(String jarPath, List<String> jarFileList) {
        this.jarPath = jarPath;
        this.jarFileList = jarFileList;
    }

    public String getJarPath() {
        return jarPath;
    }

    public void setJarPath(String jarPath) {
        this.jarPath = jarPath;
    }

    public List<String> getJarFileList() {
        return jarFileList;
    }

    public void setJarFileList(List<String> jarFileList) {
        this.jarFileList = jarFileList;
    }

    @Override
    public String toString() {
        return "JarModel{" + "jarPath='" + jarPath + '\'' + ", jarFileList=" + jarFileList + '}';
    }
}
