package sample.king.model;

import org.apache.log4j.Logger;

/**
 * Created by Pawel Miron on 04.07.2018.
 */
public class FileClassModel {

    private static final Logger LOGGER = Logger.getLogger(FileClassModel.class.getName());

    private String packageName;
    private String fullPath;

    public FileClassModel(String packageName, String fullPath) {
        this.packageName = packageName;
        this.fullPath = fullPath;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    @Override
    public String toString() {
        return "FileClassModel{" + "packageName='" + packageName + '\'' + ", fullPath='" + fullPath + '\'' + '}';
    }
}
