package sample.king.model;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pawel Miron on 04.07.2018.
 */
public class Node implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(Node.class.getName());

    private String name;
    private List<Node> children;
    private List<String> pathsToJars = new ArrayList<>();

    public Node(String name, List<Node> children) {
        this.name = name;
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Node> getChildren() {
        return children;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public List<String> getPathsToJars() {
        return pathsToJars;
    }

    public void setPathsToJars(List<String> pathsToJars) {
        this.pathsToJars = pathsToJars;
    }

    @Override
    public String toString() {
        return name + " (" + children.size() + ')';
    }
}

