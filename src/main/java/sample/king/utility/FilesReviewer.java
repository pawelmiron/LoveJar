package sample.king.utility;

import org.apache.log4j.Logger;
import sample.king.model.JarModel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

/**
 * Created by Pawel Miron on 04.07.2018.
 */
public class FilesReviewer {

    private static final Logger LOGGER = Logger.getLogger(FilesReviewer.class.getName());

    public static List<JarModel> getAllDirectoryFiles(String stringPath) throws IOException {
        Path path = Paths.get(stringPath);
        return Files.walk(path).filter(s -> s.toString().endsWith(".jar")).map(x -> new JarModel(x.toAbsolutePath().toString(), getAllFilesFromJar(x.toAbsolutePath().toString()))).sorted().collect(Collectors.toList());
    }

    private static List<String> getAllFilesFromJar(String path) {
        try (JarFile jarFile = new JarFile(path)) {
            return jarFile.stream().filter(jarEntry -> jarEntry.getName().endsWith(".class")).map(ZipEntry::getName).collect(Collectors.toList());
        } catch (IOException e) {
            LOGGER.error("Input exception", e);
            return null;
        }
    }
}
