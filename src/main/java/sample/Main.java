package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = getClass().getClassLoader().getResource("sample.fxml");
        if (url != null) {
            Parent root = FXMLLoader.load(url);
            primaryStage.setTitle("LoveJar");
            primaryStage.getIcons().add(new Image("heart.svg.png"));
            primaryStage.setScene(new Scene(root, 600, 400));
            primaryStage.show();
        } else {
            throw new NullPointerException("Path to main window fxml points to null");
        }

    }
}
